/* iOS and Android Push Notification Server Implementation *
 * ------------------------------------------------------- *
 * Name:  Ethan Setnik                                     *
 * Email: ethan.setnik@mobileaware.com                     *
 * Skype: esetnik                                          *
 * ------------------------------------------------------- */

// required modules
var express = require('express'), app = express.createServer();
var ejs = require('ejs');
var color = require('colors');

// internal dependencies
// var mysql = require('./lib/db.js'); // generic implementation
var mysql = require('./lib/ma.js');
var push = require('./lib/push_driver.js');

// setup cron to schedule delivery of push notifications on a predefined interval
var cronJob = require('cron').CronJob;

var sendPushNotification = function(notification){
	push.sendPushNotification(notification,
		function(){
			// success
			console.log("notification sent successfully");
			mysql.setPushNotificationSent(notification);
		},
		function(){
			//failure
		}
	);
};

var checkConfigs = function(config){
	if(!config || !config.db || !config.apns || !config.gcm){
		throw new Error("Configuration missing!");
	}
};

exports.Push = {
	configure : function(config){

		// check for missing configs
		checkConfigs(config);

		push.configure(config);

		this.config = config;
		return this;
	},

	// start listening on a specified port
	start : function(port){
		
		// check for missing configs
		checkConfigs(this.config);

		// connect to db instance
		mysql.connect(this.config.db);

		app.listen(port);

		// send queued notifications every 3 seconds
		new cronJob('*/3 * * * * *',
			function(){
				mysql.getPendingNotifications(
					function(notifications){
						//success
						var i;
						for(i in notifications){
							sendPushNotification(notifications[i]);
						}
					},
					function(err){
						//failure
						throw err;
					}
				);
			},null, true, null);

		return this;
	}
};

// initialize express middleware
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.methodOverride());

// set the templating engine
app.set('view engine', 'ejs');

// disable layouts
app.set('view options', {
  layout: false
});

// set the views path
app.set('views', './templates');

// Send JSON Payload with user and device_token to associate
// {"user":{"email":"test@gmail.com"},"device":{"type":"iOS","token":"54332vs4394390q3934j3490490"}}
app.post('/push/register/device', function(req, res){
	//console.log("Registering body "+JSON.stringify(req.body));
	var device = req.body;
	var uuid = new cookies(req, res).get("ASIC");
	console.log("Registering new device for push notifications");
	console.log("device="+device);
	console.log("uuid="+uuid);
	if(device){
		mysql.registerDevice(uuid,device,
		function(){
			// success echoes the request
			res.send(req.body);
		},
		function(err){
			res.send({error:err});
			throw err;
		});
	}
});

// Send JSON Payload with user info
//{"user":{"email":"test@gmail.com","first_name":"John","last_name":"Doe"}}
app.post('/push/register/user', function(req, res){
	res.send(req.body);
	var user = req.body.user;
	mysql.registerUser(user);
});

// Send JSON payload with user and message to send
//{"user":{"user_id":1},"message":"Hello there"}
app.post('/push/send', function(req, res){
	var message = req.body.message;
	var user = req.body.user;
	
	if(message && user && user.user_id){

		mysql.getDevicesForUser(user,
			function(devices){
				// successfully found devices belonging to user
				
				for(var i in devices){
					// queue a push notification to each device belonging to the user
					mysql.queuePushNotification(devices[i],message,user);
				}
			},
			function(err){
				// failed to find device
				throw err;
		});
		res.send('Sent notification');

	}
	else{
		// input error
		console.log("Improperly formatted input");
	}
});

// Render mobile web template for viewing new notifications
// Browse to /push/notifications/1 to view notifications for user 1
app.get('/push/notifications/:user_id', function(req,res){
	
	mysql.getNotificationsForUserID(req.params.user_id,function(n){
		// success
		res.render('index',{'notifications':n});

	},
	function(err){
		// failed to find user
		res.send('<h1>Error, user '+req.params.user_id+' not found!</h1><p>'+err+'</p>');
		throw err;
	});
	
});

app.get('/push/messages/get',mysql.getMessages);
app.post('/push/messages/seen',mysql.seenMessages);

app.get('/push/notifications/viewed/:notification_id', function(req,res){
	
	mysql.setPushNotificationViewed(req.params.notification_id,function(){
		// success
		res.redirect('back');

	},
	function(err){
		// failed to find user
		res.send('<h1>Error, couldn\'t remove notification</h1><p>'+err+'</p>');
		throw err;
	});
	
});

app.get('/push/notifications/count/:user_id', function(req,res){
	
	var user_id = req.params.user_id;
	if(user_id){
		mysql.getNotificationCountForUserID(user_id,function(result){
			// success
			res.send({"badge_count":result.badge_count});

		},
		function(err){
			// failed to find user
			res.send('<h1>Error, couldn\'t get notification count</h1><p>'+err+'</p>');
			throw err;
		});
	}
	
});

// Default route
// Provide basic usage information
app.get('/push', function(req,res){
	

	res.send('<h1>Usage</h1><p>To check notifications for a user browse to /push/notifications/user_id</p>');
});

 

