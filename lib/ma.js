var mysql, _mysql = require('mysql');


exports.connect = function(config){
	var user = config.user;
	var pass = config.password;
	var db = config.db;

	mysql = _mysql.createClient({
		host: config.host,
		port: config.port,
		user: config.user,
		password: config.password
	});

	mysql.query('USE ' + config.db);
};

exports.registerDevice = function(uuid,device,success,failure){
	// success
	var query = "INSERT INTO user_devices (user_id,token,type) VALUES ((SELECT id FROM user WHERE uuid=?),?,?);";
	mysql.query(query,[uuid,device.token,device.type],
		function(err, result, fields) {
			if (err){
				failure(err);
			}
			else {
				console.log("added device to user success");
				success();
			}
		}
	);
};

/* not used?
exports.registerUser = function(user){
	mysql.query("INSERT INTO users (first_name,last_name,email,badge_count) VALUES ('"+user.first_name+"','"+user.last_name+"','"+user.email+"',0) ON DUPLICATE KEY UPDATE first_name='"+user.first_name+"',last_name='"+user.last_name+"'",
	function(err, result, fields) {
		if (err) throw err;
		else {
			console.log('inserted user success');
		}
	});
};
*/

exports.getDevicesForUser = function(uuid,success,failure){ // touched
	var query =
		'SELECT d.token,d.type FROM user_devices d \n'+
		'INNER JOIN user u ON u.id=d.id \n'+
		'WHERE u.uuid=?';
	mysql.query(query,[uuid],
	function(err, result, fields) {
		if (err) {
			failure(err);
		}
		else {
			success(result);
		}
	});
};

/*
exports.getUserForEmail = function(email,success,failure){
	console.log("email = "+email);
	mysql.query("select * from users where email LIKE '"+email+"' LIMIT 1",
		function(err, result, fields) {
			if(result[0]){
				success(result[0]);
			}
			else{
				if(err){
					failure(err);
				}
				else{
					failure("user not found for email "+email);
				}
			}
		});
};

exports.getUserForUserID = function(user_id,success,failure){
	mysql.query("select * from users where user_id = "+user_id+" LIMIT 1",
	function(err, result, fields) {
		if (err) failure(err);
		else {
			success(result[0]);
		}
	});
};
*/

/* deprecated
exports.getNotificationsForUserID = function(user_id,success,failure){ // superceded by getMessages;
	mysql.query("SELECT n.notification_id,n.message FROM notifications n INNER JOIN devices d ON d.device_id = n.device_id WHERE n.viewed = 0 AND d.user_id = "+user_id,
	function(err, result, fields) {
		if (err) failure(err);
		else {
			success(result);
		}
	});
};

*/

exports.getNotificationCountForUserID = function(user_id,success,failure){ // touched
	var query = 'SELECT 1 FROM user_notification WHERE !viewed AND user_id=?';
	mysql.query(query,[user_id],
	function(err, result, fields) {
		if (err) failure(err);
		else {
			console.log(result.length);
			success(result.length);
		}
	});
};

exports.getPendingNotifications = function(success,failure){ // touched
	var query =
		'SELECT d.device_id,d.notify_id,ud.user_id,ud.token,ud.type,\n'+
		'COUNT(device_id) AS badge_count,\n'+
		'GROUP_CONCAT(d.notify_id SEPARATOR ",") as notify_ids \n'+
		'FROM device_notification d \n'+
		'INNER JOIN user_devices ud ON d.device_id=ud.id \n'+
		'WHERE !d.sent GROUP by d.device_id';

	mysql.query(query,
	function(err, result, fields) {
		var array = [],
			i, count, row, note;
		if (err) failure(err);
		else {
			for (i in result) {
				row = result[i];
				count = row.badge_count;

				note = {
					device: {
						device_id:row.device_id,
						token:row.token,
						type:row.type,
						user_id:row.user_id
					},
					message:'You have '+count+' unviewed notifications',
					notification_id:row.device_id+'-'+row.notify_ids,
					badge_count:count
				};

				array.push(note);
			}
			success(array);
		}
	});
};

exports.queuePushNotificationForAll = function(message) { // touched // verify?
	var query = "INSERT INTO notifications (message) VALUES (?)";
	mysql.query(query,[message],function(err,result,fields){
		if (err) throw err;
		else {
			query="INSERT IGNORE INTO device_notification (device_id, notify_id) SELECT id, ? FROM user_devices";
			mysql.query(query,[result.insertId],function(err,result,fields){
				if (err) throw err;
				else {console.log(("added notification for "+result.affectedRows+" users").blue.bold);return (result.affectedRows);}
			});
		}
	});
};

/*
exports.queuePushNotification = function(device,message,user){ // fixish

	// add a new notification to the queue
//	mysql.query("INSERT INTO notifications (device_id,message,sent,viewed) VALUES ("+device.device_id+",'"+message+"',0,0)",
	var query = "INSERT INTO user_notifications (user_id,notify_id) VALUES (?,?)";
	mysql.query(query,[user_id,notify_id],
	function(err, result, fields) {
		if (err) throw err;
		else {
			console.log('queued notification success');
		}
	});

 unnecessary?

	// update the badge value for the user
	mysql.query("UPDATE users SET badge_count = badge_count+1 WHERE user_id="+user.user_id,
	function(err, result, fields) {
		if (err) throw err;
		else {
			console.log('updated badge_count success');
		}
	});
};
*/

exports.setPushNotificationSent = function(notification){ // touched
	var ids = notification.notification_id.split('-'),
		query =
			"UPDATE device_notification \n"+
			"SET sent=1 \n"+
			"WHERE device_id=? AND notify_id IN ("+ids[1]+")";
	mysql.query(query,[ids[0]],
	function(err, result, fields) {
		if (err) throw err;
		else {
			console.log('updated Pushnotification sent status success');
		}
	});
};

/*
exports.setPushNotificationViewed = function(notification_id,success,failure){ // touched
	
	// Mark the notification as viewed
//	mysql.query("UPDATE notifications SET viewed=1 WHERE notification_id ="+notification_id,
	var ids = notification_id.split('-'),
		query =
			"UPDATE user_notification \n"+
			"SET viewed=1 \n"+
			"WHERE user_id=? AND notify_id=?";
//	mysql.query("UPDATE notifications SET sent=1 WHERE notification_id ="+notification.notification_id,
	mysql.query(query,ids,
	function(err, result, fields) {
		if (err) failure(err);
		else {
			console.log('updated notification viewed status success');
			success();
		}
	});

	// Decrement the badge count

 unnecessary?

	mysql.query("SELECT u.user_id FROM users u INNER JOIN devices d ON u.user_id = d.user_id INNER JOIN notifications n ON n.device_id = d.device_id WHERE n.notification_id="+notification_id,
	function(err, result, fields) {
		if (err) failure(err);
		else {
			var user_id = result[0].user_id;
			mysql.query("UPDATE users SET badge_count=badge_count-1 WHERE user_id ="+user_id,
			function(err, result, fields) {
				if (err) failure(err);
				else {
					console.log('updated notification viewed status success');
				}
			});
		}
	});
};
*/

// DW code here //

exports.getMessages = function (req, res) { // ok
	var query="SELECT n.id,n.message \n"+
		"FROM user_notification un \n"+
		"INNER JOIN notifications n \n"+
		"ON un.notify_id=n.id \n"+
		"WHERE un.user_id=(SELECT id from user where uuid=? LIMIT 1) \n"+
		"AND !un.viewed";
	console.log("GET MESSAGES FOR ME! uuid: "+req.cookies.asic);
	mysql.query(query,[req.cookies.asic],
	function(err, result, fields) {
		res.header("Cache-Control","no-cache");
		if (err) res.json({error:err},400);
		else {
			console.log(result);
			res.json({results:result});
		}
	});
};

exports.seenMessages = function (req, res) { // ok
	var ids = req.body.notify_ids && req.body.notify_ids.split(','),
		query =
		"UPDATE user_notification \n"+
		"SET viewed=1 \n"+
		"WHERE user_id=(SELECT id from user where uuid=? LIMIT 1) \n"+
		"AND notify_id IN ("+ids.map(function(x){return parseInt(x,10);})+")";
	mysql.query(query,[req.cookies.asic],
	function(err, result, fields) {
		if (err) res.json({error:err},400);
		else {
			res.send(204);
			console.log('updated SEEN MESSAGES');
		}
	});
};