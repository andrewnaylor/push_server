
// Requires
var apns = require('apn');
var GCM = require('gcm').GCM;
var gcm,apnsConnection;

// Setup Apple Push Notification Server
exports.configure = function(config){
	
	// Setup iOS Apple Push Notification Server

	// map apns and gcm errors to the error callback
	config.apns.error = config.error;
	config.gcm.error = config.error;

	apnsConnection = new apns.Connection(config.apns);

	gcm = new GCM(config.gcm.apiKey);

};


exports.sendPushNotification = function(notification,success,failure){
	var sendTheNotify = {};
	switch (notification.device.type) {
	case 2: sendTheNotify = sendAndroidPushNotification; break;
	case 1: sendTheNotify = sendiOSPushNotification; break;
	default: failure({error:error}); return;
	}
	sendTheNotify(notification,
		function(result){success(notification);},
		function(err){failure(err);}
	);
};

function sendiOSPushNotification(notification, success, failure){
	console.log("sent push notification to iOS device: "+notification.device.device_id);
	var myDevice = new apns.Device(notification.device.token /*, ascii=true*/);
	
	var note = new apns.Notification();
	console.log("Notification badge count "+notification.badge_count);
	note.badge = notification.badge_count;
	note.sound = "ping.aiff";
	note.alert = notification.message;
	note.payload = {user_id:notification.device.user_id,notification_id:notification.notification_id};

	apnsConnection.pushNotification(note, myDevice);
	
	success();
}

function sendAndroidPushNotification(notification, success, failure){
	var message = {
		registration_id: notification.device.token,
		collapse_key: 'OneStop', // required
		'data.notification_id': notification.notification_id,
		'data.user_id': notification.device.user_id,
		'data.message': notification.message
	};

	console.log("Message:"+JSON.stringify(message));

	gcm.send(message,
		function(err, messageId){
			if (err) {
				console.log("Something has gone wrong: ".red+err.red.bold);
				failure(err);
			} else {
				console.log("Sent with message ID: ", messageId);
				success();
			}
		}
	);
}