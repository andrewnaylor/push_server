var mysql, _mysql = require('mysql');


exports.connect = function(config){
	var user = config.user;
	var pass = config.password;
	var db = config.db;

	mysql = _mysql.createClient({user: user,password: pass});

	mysql.query('use ' + db);
};

exports.registerDevice = function(user,device,success,failure){
	
	// TODO: escape possible js the user could maliciously insert
	this.getUserForEmail(user.email,
		function(user){
			// success
			mysql.query("insert into devices (user_id,token,type) values ('"+user.user_id+"','"+device.token+"','"+device.type+"') ON DUPLICATE KEY UPDATE user_id="+user.user_id,
				function(err, result, fields) {
					if (err) {
						failure(err);
					} else {
						console.log("added device to user success");
						success();
					}
				}
			);
		},
		function(err){
			//failure
			failure(err);
		}
	);
	
	
};

exports.registerUser = function(user){
	mysql.query("INSERT INTO users (first_name,last_name,email,badge_count) VALUES ('"+user.first_name+"','"+user.last_name+"','"+user.email+"',0) ON DUPLICATE KEY UPDATE first_name='"+user.first_name+"',last_name='"+user.last_name+"'",
	function(err, result, fields) {
		if (err) {
			throw err;
		} else {
			console.log('inserted user success');
		}
	});
};

exports.getDevicesForUser = function(user,success,failure){
	mysql.query("select d.token,d.device_id from devices d inner join users u on u.user_id=d.user_id where u.user_id="+user.user_id,
	function(err, result, fields) {
		if (err) {
			failure(err);
		} else {
			success(result);
		}
	});
};

exports.getUserForEmail = function(email,success,failure){
	console.log("email = "+email);
	mysql.query("select * from users where email LIKE '"+email+"' LIMIT 1",
		function(err, result, fields) {
			if(result[0]){
				success(result[0]);
			} else{
				if(err){
					failure(err);
				} else{
					failure("user not found for email "+email);
				}
			}
		});
};

exports.getUserForUserID = function(user_id,success,failure){
	mysql.query("select * from users where user_id = "+user_id+" LIMIT 1",
	function(err, result, fields) {
		if (err) failure(err);
		else {
			success(result[0]);
		}
	});
};


exports.getNotificationsForUserID = function(user_id,success,failure){
	mysql.query("SELECT n.notification_id,n.message FROM notifications n INNER JOIN devices d ON d.device_id = n.device_id WHERE n.viewed = 0 AND d.user_id = "+user_id,
	function(err, result, fields) {
		if (err) failure(err);
		else {
			success(result);
		}
	});
};

exports.getNotificationCountForUserID = function(user_id,success,failure){
	mysql.query("SELECT badge_count FROM users WHERE user_id = "+user_id,
	function(err, result, fields) {
		if (err) failure(err);
		else {
			console.log(result[0]);
			success(result[0]);
		}
	});
};

exports.getPendingNotifications = function(success,failure){
	mysql.query("select d.device_id,d.token,d.type,d.user_id,n.message,n.notification_id,u.badge_count from notifications n inner join devices d on n.device_id=d.device_id INNER JOIN users u on u.user_id=d.user_id where n.sent=0",
	function(err, result, fields) {
		if (err) failure(err);
		else {
			var array = [];
			for(var i in result){
				var res = result[i];
				var device = {
					device_id:res.device_id,
					token:res.token,
					type:res.type,
					user_id:res.user_id
				};

				var note = {
					device:device,
					message:res.message,
					notification_id:res.notification_id,
					badge_count:res.badge_count
				};

				array.push(note);
			}
			success(array);
		}
	});
};

exports.queuePushNotification = function(device,message,user){

	// add a new notification to the queue
	mysql.query("INSERT INTO notifications (device_id,message,sent,viewed) VALUES ("+device.device_id+",'"+message+"',0,0)",
	function(err, result, fields) {
		if (err) throw err;
		else {
			console.log('queued notification success');
		}
	});

	// update the badge value for the user
	mysql.query("UPDATE users SET badge_count = badge_count+1 WHERE user_id="+user.user_id,
	function(err, result, fields) {
		if (err) throw err;
		else {
			console.log('updated badge_count success');
		}
	});
};

exports.setPushNotificationSent = function(notification){
	mysql.query("UPDATE notifications SET sent=1 WHERE notification_id ="+notification.notification_id,
	function(err, result, fields) {
		if (err) throw err;
		else {
			console.log('updated notification sent status success');
		}
	});
};

exports.setPushNotificationViewed = function(notification_id,success,failure){
	
	// Mark the notification as viewed
	mysql.query("UPDATE notifications SET viewed=1 WHERE notification_id ="+notification_id,
	function(err, result, fields) {
		if (err) failure(err);
		else {
			console.log('updated notification viewed status success');
			success();
		}
	});

	// Decrement the badge count


	mysql.query("SELECT u.user_id FROM users u INNER JOIN devices d ON u.user_id = d.user_id INNER JOIN notifications n ON n.device_id = d.device_id WHERE n.notification_id="+notification_id,
	function(err, result, fields) {
		if (err) failure(err);
		else {
			var user_id = result[0].user_id;
			mysql.query("UPDATE users SET badge_count=badge_count-1 WHERE user_id ="+user_id,
			function(err, result, fields) {
				if (err) failure(err);
				else {
					console.log('updated notification viewed status success');
				}
			});
		}
	});
};